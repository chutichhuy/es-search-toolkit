var elasticsearch = require('elasticsearch');
var objectAssign  = require('object-assign');
var Handlebars    = require('handlebars');
var Promise       = require('bluebird');

var fs = require('fs');

var client = new elasticsearch.Client({
  host: '10.100.0.105:9200'
});

var queryTpl = fs.readFileSync('./query2.hdb').toString('utf8');
var queryOld = fs.readFileSync('./query-old.hdb').toString('utf8');

var qFn     = Handlebars.compile(queryTpl);
var qFnOld  = Handlebars.compile(queryOld);

// top
var top = require('./files/top.json');

var search = function (body) {
    return client.search({
        index: 'product',
        body: body
    });
};

var getResultNames = function (resp) {
    return resp.hits.hits.map(function (h) {
        return h._source.meta_title;
    }).join('\n');
};

var nSpaces = function (n) {
    return (function doSpace(c) {
        return (c.length < n) ? doSpace(c + " ") : c;
    })('');
};

top.forEach(function (kw) {
    var qs = [qFn, qFnOld].map(function (f) {
        return JSON.parse(f({keyword: kw}));
    }).map(search);

    Promise.all(qs).then(function (rs) {
        return rs.map(function (r) {
            return r.hits.hits.map(function (h) {
                return h._source.meta_title;
            });
        });
    }).then(function (rs) {

        var longest = rs[0].reduce(function (p, c) {
            return c && (c.length > p) ? c.length : p;
        }, 0);

        // now combine the two arrays
        var content = rs[0].map(function (r, index) {
            var rlen  = r ? r.length : 0;
            var newRs = (r && (rlen === longest)) ? r : (r + nSpaces(longest - rlen));
            return [newRs, rs[1][index]];
        }).map(function (l) {
            return l.join(' | ');
        }).join('\n');

        // now write 
        //
        fs.writeFileSync('./out/compare/' + kw + '.txt', content);
    });
});
