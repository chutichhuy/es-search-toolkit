var program       = require('commander');
var elasticsearch = require('elasticsearch');
var objectAssign  = require('object-assign');

var Handlebars   = require('handlebars');

var fs = require('fs');

var client = new elasticsearch.Client({
  host: '10.100.0.105:9200'
  //host: 'localhost:9200'
});

program
  .version('0.0.1')
  .option('-k, --keyword <keyword>', 'Keyword to search')
  .parse(process.argv);

var queryTpl = fs.readFileSync('./query2.hdb').toString('utf8');
var qFn = Handlebars.compile(queryTpl);

var body = JSON.parse(qFn({keyword: program.keyword}));

console.log('Searching for keyword: "' + program.keyword + "'");

client.search({
    index: 'product',
    type: 'product',
    body: body
}, function (err, resp) {
    if (err) {
        console.log('error', err);
        return;
    }

    var r = resp.hits.hits.map(function (h) {
        var s = h._source;
        return s.meta_title +   ' | '
            + s.salable +       ' | '
            + s.productset_id + ' | '
            + h._score;
    }).join('\n');

    fs.writeFileSync('./out/search.txt', (r));
    fs.writeFileSync('./out/search-row.json', JSON.stringify(resp.hits.hits));
    console.log("Done!");
});
