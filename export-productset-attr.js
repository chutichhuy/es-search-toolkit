var mysql = require('mysql');
var fs    = require('fs');

var connection = mysql.createConnection({
  host     : '10.100.0.105',
  user     : 'huy.nguyen',
  password : 'ebhsk6qrbmndkotn',
  database : 'tala_migration'
});

new Promise(function (resFn, rejFn) {
    connection.query('SELECT * FROM catalog_productset', function (err, rows) {
        connection.query('SELECT * FROM catalog_productset_attribute', function (err, attrs) {
            var das = attrs.filter(function (attr) {
                return attr.productset_id == 4;
            }).map(function (attr) {
                return attr.attribute_code;
            }).concat([
                'miki_price', 
                'video', 
                'promotion',
                'price_usd',
                'supplier',
                'origin', 
                'kieu_dang_gift',
                'warranty_lifetime',
                'warranty_and_service',
                'express',
                'express_vietnamese',
                'vat_taxable',
                'item_model_number',
                'display_remain_day_special_price',
                'dimensions',
                'manufacturer_clother',
                'tiki_recommendation',
                'included_accessories',
                'edition',
                'product_weight',
                'brand',
                'chatluonhchuot',
                'ngayhethan',
                'dangiakhac',
                'anhchuot',
                'giathue',
                'is_applicable_shopping_cart_rule',
                'manufacturers_gift',
                'plastic_cover_suitable',
                'is_genuine',
                'only_ship_to',
                'expiry_time',
                'product_feature',
                'bulky',
                'quy_cach_san_pham',
                'requested_tax'
            ]);

            resFn(rows.filter(function (ps) {
                return ps.id != 4;
            }).map(function (ps) {
                return {
                    productset: ps.name,
                    attributes: attrs.filter(function (attr) {
                        return attr.productset_id == ps.id;
                    }).map(function (attr) {
                        return attr.attribute_code;
                    }).filter(function (attr) {
                        return das.indexOf(attr) == -1;
                    })
                };
            }));
        });
    });
}).then(function (result) {
    fs.writeFileSync('./out/productset-attribute.json', JSON.stringify(result));
});
