var elasticsearch = require('elasticsearch');
var objectAssign  = require('object-assign');

var client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});

var ins = [
    {
        name: 'alison wonderland dj trap',
        age: 25,
        id: 1
    },
    {
        name: 'samurai dj trap hip hop',
        age: 42,
        id: 2
    },
    {
        name: 'jack wide rockstar',
        age: 51,
        id: 3
    },
    {
        name: 'dj robot',
        age: 21,
        id: 4
    },
    {
        name: 'james hetfield',
        age: 56,
        id: 5
    }, 
    {
        name: 'hếtfield',
        age: 59,
        id: 6
    },
    {
        name: 'tráp',
        age: 61,
        id: 7
    }
];

ins.forEach(function (i) {
    client.create({
        index: 'play2',
        type: 'play',
        body: i
    });
});

