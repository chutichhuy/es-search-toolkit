var csvParser = require('csv-parse');
var fs        = require('fs');

var content = fs.readFileSync('./files/ces.csv').toString('utf8');

csvParser(content, function (err, data) {
    var top = data.filter(function (l) {
        return l[1] == 4;
    }).sort(function (a, b) {
        return a[4] < b[4];
    }).slice(0, 100).map(function (l) {
        return l[2];
    });

    fs.writeFileSync('./files/top.json', JSON.stringify(top));
});
