var mysql = require('mysql');

var connection = mysql.createConnection({
  host     : '10.100.0.105',
  user     : 'huy.nguyen',
  password : 'ebhsk6qrbmndkotn',
  database : 'tala_migration'
});

var nameNotNull = function (p) {
    return p.name != null;
};

var nameOnly = function (p) {
    return  p.name;
};

var removeNumber = function (name) {
    return name.replace(/[0-9]/g, '');
};

var toLower = function (name) {
    return ' ' + name.toLowerCase() + ' ';
};

var createBrandRemover = function (brands) {
    return function (name) {
        return brands.reduce(function (p, c) {
            return p.replace(c, ' ');
        }, name);
    };
};

connection.connect(function (err) {
    if (err) {
        console.log('can not connect to Mysql');
        return;
    }

    var parseProductName = function (brandNames) {
        connection.query('SELECT name FROM catalog_product WHERE productset_id NOT IN (26, 31)', function (err, rows) {
            var names = rows.filter(nameNotNull)
                .map(nameOnly)
                .map(toLower)
                .map(removeNumber)
                .map(createBrandRemover(brandNames));
            console.log(names);
        });
    };

    connection.query('SELECT name FROM catalog_brand', function (err, rows) {
        var names = rows.filter(nameNotNull).map(nameOnly).map(toLower);
        parseProductName(names);
    });
});
