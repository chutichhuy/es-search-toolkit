var program       = require('commander');
var elasticsearch = require('elasticsearch');
var objectAssign  = require('object-assign');

var Handlebars   = require('handlebars');

var fs = require('fs');

var client = new elasticsearch.Client({
    host: 'localhost:9200'
});

var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://10.100.0.105:27017/tala';

var unmarked =  function (str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    return str;
};

var create = function (data, count) {
    if (count >= data.length) {
        console.log('DONE');
        return;
    }
    var p = data[count];

    client.create({
        index: 'product',
        type: 'product',
        id: count,
        body: {
            searchable_name: p.searchable_name,
            // "searchable_name.unmarked": unmarked(p.searchable_name)
            searchable_author: p.searchable_author,
            meta_title: p.search_name
            //"searchable_author.unmarked": unmarked(p.searchable_author)
        }
    }, function (err) {
        create(data, count + 1);
    });
};

MongoClient.connect(url, function(err, db) {
    console.log("Connected correctly to server.");
        
    var productCol = db.collection('product');
    productCol.find().limit(3000).skip(10).toArray(function (err, products) {
        create(products, 0);
        db.close();
    });
});
