# HOW TO

1. clone the repo
2. ```npm install```

## Generate results comparation 

```node ./compare.js```

The result files will be put in ```out/compare```

## Search for some keyword

```node ./search.js -k "keyword go here"```

Look for results in ```out/search.txt```

