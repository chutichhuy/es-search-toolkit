var program       = require('commander');

var elasticsearch = require('elasticsearch');
var objectAssign  = require('object-assign');

var Handlebars   = require('handlebars');

var fs = require('fs');

var client = new elasticsearch.Client({
  host: '10.100.0.105:9200'
  //host: 'localhost:9200'
});

var csvParser = require('csv-parse');

var Promise = require('bluebird');

program
  .version('0.0.1')
  .option('-q, --query <query>', 'Query template')
  .parse(process.argv);

var queryTpl = fs.readFileSync(program.query).toString('utf8');
var qFn = Handlebars.compile(queryTpl);

var testKeywords = fs.readFileSync('./test-keywords.csv').toString('utf8');

var testAKeyword = function (keyword, expects) {
    return new Promise(function (resFn, rejFn) {
        client.search({
            index: 'product',
            type: 'product',
            body: qFn({keyword: keyword})
        }, function (err, resp) {

            if (err) {
                rejFn(false);
                return;
            }
            
            var matched = resp.hits.hits.map(function (h) {
                return h._source.id;
            });
            
            // now compare
            resFn({
                result: expects.map(function (id) {
                        return (matched.indexOf(id) > -1);
                    }).reduce(function (r, c) {
                        return c ? true : false;
                    }, true),
                keyword: keyword
            });
        });
    });
};

var notNull = function (i) {
    return i != '';
};

var trim = function (i) {
    return parseInt(i.trim());
};

csvParser(testKeywords, function (err, keywords) {
    var tests = keywords.map(function (item) {
        return {
            keyword: item[0],
            expects: item.slice(1).filter(notNull).map(trim)
        };
    });

    var prms = tests.map(function (c) {
        return testAKeyword(c.keyword, c.expects);
    });

    Promise.all(prms).then(function (results) {
        var result = results.reduce(function (p, c) {
            return c.result ? true : false;
        }, true);

        if (result) {
            console.log('PASSED');
        } else {
            console.log('FAILED');
            results.forEach(function (c) {
                if (!c.result) {
                    console.log(c.keyword);
                }
            });
        }
    }).catch(function (err) {
        console.log('err', err);
    });
});
